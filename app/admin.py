'''
Created on 09/01/2013

@author: moisesguimaraes
'''
import jinja2, webapp2

from app.models import Set, Card
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader('templates'))


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):

    def post(self):
        upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
        blob_info = upload_files[0]
        
        card_set = Set.get_by_key_name(self.request.get('trigram'))
        
        if card_set:
            self.response.out.write('Set already exists...')
            
            blob_info.delete()            
        else:
            card_set = Set(key_name=self.request.get('trigram'), name=self.request.get('name'), blob=blob_info.key())
            card_set.put()
            
            blob = blob_info.open()
            
            count = 0;
            for line in blob.readlines():
                number, name = line.split(';')
                
                card = Card.new(int(number.strip()), name.strip(), card_set)
                card.put()
                count += 1
                
            self.response.out.write('%03d cards imported.' % count) 

        self.response.out.write('<a href="/admin/dashboard">Dasboard</a>')

class DashboardHandler(webapp2.RequestHandler):
    
    def get(self):
        template_values = {'action' : blobstore.create_upload_url('/admin/upload'),
                           'sets'   : Set.all().fetch(10)}
        
        template = jinja_environment.get_template('admin/upload.html')
        self.response.out.write(template.render(template_values))


app = webapp2.WSGIApplication([('/admin/dashboard', DashboardHandler),
                               ('/admin/upload', UploadHandler)], debug=True)
