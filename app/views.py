'''
Created on 21/01/2013

@author: moisesguimaraes
'''
import webapp2
from app.models import Set
from app.handlers import templates


class CardSearchHandler(webapp2.RequestHandler):
    
    def get(self):
        template = templates.get_template('cards/search.html')
        values = {'sets' : Set.all().fetch(100)}
        
        self.response.out.write(template.render(values))
        

app = webapp2.WSGIApplication([('/cards/', CardSearchHandler)], debug=True)
