'''
Created on 07/01/2013

@author: moisesguimaraes
'''
from tests import TestCase
from app.models import Account

class Test(TestCase):

    def test_creation(self):
        fetched = Account.all().fetch(1)[0]
        
        self.assertEquals(fetched.google_user.email(), self.email)
